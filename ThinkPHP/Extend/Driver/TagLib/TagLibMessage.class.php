<?php
class TagLibMessage extends TagLib{
	protected $tags= array(
		//定义标签
		'code'=>array(
			'attr'=>'width,height',
			'close'=>0,
		),
	);

	public function _code($attr)   {
		$tag    = $this->parseXmlAttr($attr,'code');
		if(isset($tag['width'])){
			$width   =   $tag['width'];
		}else{
			$width=30;		
		}
		if(isset($tag['height'])){
			$height    =    $tag['height'];
		}else{
			$height=30;
		}
		$str ="<img src='__APP__/Public/verify?w={$width}&h={$height}' onclick='this.src=this.src+\"?\"+Math.random()' >";
		return $str;
	}
}
?>
