<?php
class LoginAction extends Action{
	public function login(){
		$this->display();	
	}

	public function doLogin(){
		//接受前台的值
		//判断用户是否存在，密码是否正确
		$username=$_POST['username'];
		$password=$_POST['password'];
		$verify=$_POST['verify'];
		if(md5($verify)!=$_SESSION['verify']){
			$this->error('验证码错误');
		}
		$user=M('User');
		$where['username']=$username;
		$where['password']=$password;
		$arr=$user->where($where)->find();
		if($arr){
			$_SESSION['username']=$username;
			$_SESSION['id']=$arr['id'];
			$this->success('登录成功',U('Index/index'));
		}else{
			$this->error('登录失败');	
		}
	}
	
	public function doLogout(){
		$_SESSION=array();
		if(isset($_COOKIE[session_name()])){
			setcookie(session_name(),'',time()-1,'/');
		}
		session_destroy();
		$this->redirect("__APP__/Index/index");
	}

}
?>
